# ILST: LIMS for Students #

This project is about building a webinterface where diagnostics students 
can put in their data from scientific experiments and look at the data they put in during the project.  
Teachers can use the system to look at what the students put in and their way of thinking.

## Installation ##

* Clone the project,
* run the gradle `war` build task: `./gradlew war` and
* place the resulting war file (`./build/libs/<project>-<version>.war`) in a container engine (i.e. Tomcat)  
Or open the project in IntelliJ and add a run configuration.
* Run the `create_database.sql` file: `src/main/resources/create_database.sql` on a database of your own choosing, 
which you have a .my.cnf on in your own home directory.

## Usage ##

Run the war file and create an account or use one of the two test accounts (`username=test_team; password=team; role=TEAM`,
`username=test_teacher; password=teacher; role=TEACHER`).  
Add data using the `res_invoer`page and look at it using the `show_res`page. 

## Screenshots ##
### Login ###
![Login Form](project_management/presentation/1_login.png)

### Signup ###
![Signup Form](project_management/presentation/5_signup_filled.png)

### Add results
![results Form](project_management/presentation/10_resultsform_filled_correct_part1.png)
![results Form](project_management/presentation/11_resultsform_filled_correct_part2.png)

### Look at results ###
#### On website ####
![look at results](project_management/presentation/16_bekijk_results_uitgeklapt.png)

#### In a csv file ####
![data export](project_management/presentation/17_dataexport.png)

## Built With ##
- IntelliJ IDEA

## Features ##
This program can add scientific data to a database that is configured to host the data (using the provided `create_database.sql`).
It can be used to check on students in teams and what their thoughts are on specific experiments.  
It can also be used by students to learn to work with a laboratory management system.  
The database this program uses can store data for multiple years and projects, 
so it can also be used to get a view of what multiple years of students think of their experiments.

## License ##
Licensed under GPLv3. See gpl.md
