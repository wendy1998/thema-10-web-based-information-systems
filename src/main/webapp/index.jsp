<%--
  Created by IntelliJ IDEA.
  User: wendy
  Date: 29-11-2018
  Time: 10:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="includes/head.jsp">
  <jsp:param name="pageTitle" value="login"></jsp:param>
</jsp:include>
  <body>
    <jsp:include page="includes/logo.jsp"></jsp:include>
    <div class="row inhoud">
      <div class="col">

      </div>
      <div class="col">
        <h1>Login</h1>
        <p>Nog geen account? klik dan <a href="create_account.jsp">hier</a>.<br/>
        <p class="red">${requestScope.errorMessage}</p>
        <form action="login.do" method="post">
          <label>Login naam&nbsp;&nbsp;</label><input type="text" name="username"/><br/>
          <label>Wachtwoord&nbsp;&nbsp;</label><input type="password" name="password"/><br/>
          <input type="submit" name="submit" value="submit" id="submit"/>
        </form>
      </div>
      <div class="col">

      </div>
    </div>
    <jsp:include page="includes/footer.jsp"></jsp:include>
  </body>
</html>
