<%--
  Created by IntelliJ IDEA.
  User: wbvandermeulen
  Date: 12-12-18
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <jsp:include page="includes/head.jsp">
        <jsp:param name="pageTitle" value="resultaat invoer"></jsp:param>
    </jsp:include>
    <script src="js/toggle_visibility.js"></script>
</head>
<body>
<jsp:include page="includes/header.jsp"></jsp:include>
    <div class="row inhoud">
        <div class="col">
            <h3>Vergaarde gegevens</h3>
            <div class="row">
                <div class="col border-right">
                    <div class="row line">
                        <div class="col">
                            <p>Creatinine</p>
                            <p>&nbsp;&nbsp;In urine</p>
                            <p>
                                <label>&nbsp;&nbsp;&nbsp;&nbsp;Per liter
                                    <input type="checkbox" name="cr_ur_lit_check" id="cr_ur_lit_check"></label><br/>
                                <label>&nbsp;&nbsp;&nbsp;&nbsp;Per dag
                                    <input type="checkbox" name="cr_ur_dag_check" id="cr_ur_dag_check"></label>
                            </p>
                            <label>&nbsp;&nbsp;In bloed
                                <input type="checkbox" name="cr_bl_check" id="cr_bl_check"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p>Ureum</p>
                            <p>
                                <label>&nbsp;&nbsp;Per liter
                                    <input type="checkbox" name="ur_lit_check" id="ur_lit_check"></label><br/>
                                <label>&nbsp;&nbsp;Per dag
                                    <input type="checkbox" name="ur_dag_check" id="ur_dag_check"></label>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <p><br/><br/>
                        <label>Hematocriet
                            <input class="overig" type="checkbox" name="hematocriet_check" id="hematocriet_check"></label><br/>
                        <label>Hemoglobine
                            <input class="overig" type="checkbox" name="hemoglobine_check" id="hemoglobine_check"></label><br/>
                        <label>Diffen
                            <input type="checkbox" name="diffen_check" id="diffen_check"></label><br/>
                        <label>Salicylzuur
                            <input class="overig" type="checkbox" name="salicylzuur_check" id="salicylzuur_check"></label><br/>
                        <label>LDH
                            <input class="overig" type="checkbox" name="LDH_check" id="LDH_check"></label><br/>
                        <label>MCV
                            <input class="overig" type="checkbox" name="MCV_check" id="MCV_check"></label><br/>
                        <label>MCH
                            <input class="overig" type="checkbox" name="MCH_check" id="MCH_check"></label><br/>
                        <label>MCHC
                            <input class="overig" type="checkbox" name="MCHC_check" id="MCHC_check"></label><br/>
                    </p>
                </div>
            </div>
        </div>
        <div class="col">
            <h1>Verkregen data</h1>
            <c:if test="${requestScope.errorMessage != null}">
                <p class="red">${requestScope.errorMessage}</p>
            </c:if>
            <c:if test="${requestScope.successMessage != null}">
                <p class="green">${requestScope.successMessage}</p>
            </c:if>
            <form action="processdata" method="post" id="results">
                <div class="form-group">
                    <label>Naam analist*
                    <input type="text" class="form-control" id="analist" name="analist" required></label>
                </div>
                <div class="form-group">
                    <p>Gegevens sample</p>
                    <label>&nbsp;&nbsp;Sample label*
                    <input type="text" class="form-control" id="slabel" name="slabel" required></label>
                    <label>&nbsp;&nbsp;Datum binnenkomst*
                    <input type="date" class="form-control" id="sdatbinnen" name="sdatbinnen" required></label>
                    <label>&nbsp;&nbsp;Tijd binnenkomst
                    <input type="time" class="form-control" id="stijdbinnen" name="stijdbinnen"></label>
                    <label>&nbsp;&nbsp;Type*
                    <select class="form-control" id="stype" name="stype" required>
                        <option>bloed</option>
                        <option>urine</option>
                    </select></label>
                </div>
                <div class="form-group">
                    <p>Gegevens patient</p>
                    <label>&nbsp;&nbsp;Patient nummer*
                    <input type="number" class="form-control" id="pnum" name="pnum" required></label>
                    <label>&nbsp;&nbsp;Naam patient
                    <input type="text" class="form-control" id="pnaam" name="pnaam"></label>
                    <label>&nbsp;&nbsp;Geboorte datum*
                    <input type="date" class="form-control" id="pgebdatum" name="pgebdatum" required></label>
                </div>
                <div class="form-group hidden" id="cr_div">
                    <p>Creatinine</p>
                    <p class="hidden" id="cr_uri_txt">&nbsp;&nbsp;In urine</p>
                    <label class="hidden" id="cr_ur_lit_txt">&nbsp;&nbsp;&nbsp;&nbsp;Per liter
                        <input type="number" step="any" class="form-control" name="cr_ur_lit"></label>
                    <label class="hidden" id="cr_ur_dag_txt">&nbsp;&nbsp;&nbsp;&nbsp;Per dag
                        <input type="number" step="any" class="form-control" name="cr_ur_dag"></label>
                    <label class="hidden" id="cr_bloed_txt">&nbsp;&nbsp;In bloed
                        <input type="number" step="any" class="form-control" name="cr_bloed"></label>
                </div>
                <div class="form-group hidden" id="ureum_div">
                    <p>Ureum</p>
                    <label class="hidden" id="ur_lit">&nbsp;&nbsp;Per liter
                        <input type="number" step="any" class="form-control" name="ur_lit"></label>
                    <label class="hidden" id="ur_dag">&nbsp;&nbsp;Per dag
                        <input type="number" step="any" class="form-control" name="ur_dag"></label>
                </div>
                <div class="hidden form-group" id="diffen">
                    <div class="form-group no_line p-0 m-0">
                        <p>Diffen</p>
                    </div>
                    <div class="form-row">
                        <div class="form-group col no_line">
                            <label>Myeloblasten
                            <input type="number" step="any" class="form-control" name="difMyeloblasten"></label>
                            <label>Myelocyten
                                <input type="number" step="any" class="form-control" name="difMyelocyten"></label>
                        </div>
                        <div class="form-group col no_line">
                            <label>Promyeloblasten
                                <input type="number" step="any" class="form-control" name="difPromyeloblasten"></label>
                            <label>Metamyelocyten
                                <input type="number" step="any"  class="form-control" name="difMetamyelocyten"></label>
                        </div>
                    </div>
                    <div class="form-row">
                        <label>Neutrofiele granulocyten
                            <input type="text" id="Neutrofiele_granulocyten" class="form-control" name="difNeutrofiele_granulocyten"></label>
                    </div>
                    <div class="form-row">
                        <div class="form-group col no_line">
                            <label>Eosinofielen
                                <input type="number" step="any" class="form-control" name="difEosinofielen"></label>
                            <label>Monocyten
                                <input type="number" step="any" class="form-control" name="difMonocyten"></label>
                            <label>Plasmacellen
                                <input type="number" step="any" class="form-control" name="difPlasmacellen"></label>
                        </div>
                        <div class="form-group col no_line">
                            <label>Basofielen
                                <input type="number" step="any" class="form-control" name="difBasofielen"></label>
                            <label>Lymfocyten
                                <input type="number" step="any" class="form-control" name="difLymfocyten"></label>
                            <label>Erytroblasten
                                <input type="number" step="any" class="form-control" name="difErytroblasten"></label>
                        </div>
                    </div>
                </div>
                <div class="hidden form-group" id="overig_div">
                    <p>Overig</p>
                    <label class="hidden" id="Hematocriet">Hematocriet
                        <input type="number" step="any" class="form-control" name="Hematocriet"></label>
                    <label class="hidden" id="Hemoglobine">Hemoglobine
                        <input type="number" step="any" class="form-control" name="Hemoglobine"></label>
                    <label class="hidden" id="Salicylzuur">Salicylzuur
                        <input type="number" step="any" class="form-control" name="Salicylzuur"></label>
                    <label class="hidden" id="LDH">LDH
                        <input type="number" step="any" class="form-control" name="LDH"></label>
                    <label class="hidden" id="MCV">MCV
                        <input type="number" step="any" class="form-control" name="MCV"></label>
                    <label class="hidden" id="MCH">MCH
                        <input type="number" step="any" class="form-control" name="MCH"></label>
                    <label class="hidden" id="MCHC">MCHC
                        <input type="number" step="any" class="form-control" name="MCHC"></label>
                </div>
                <div class="form-group">
                    <textarea form="results" name="note" cols="50" rows="6" placeholder="note"></textarea>
                    <label>Dit is een revisie <input type="checkbox" name="revision" value="revision"/></label>
                </div>
                <input id="submit" type="submit" value="Submit" name="submit"/>
            </form>
        </div>
        <div class="col">
            <p>Welkom ${sessionScope.user.userName}!
            <form action="logout.do" method="POST">
                <input type="submit" id="logout" name="logout" value="logout">
            </form>
            </p>
        </div>
        <script type="text/javascript">
            if(document.readyState) {
                window.setInterval(function () {
                    check_creatinine();
                    check_ureum();
                    check_diffen();
                    check_others();
                }, 10);
            }
        </script>
    </div>
    <jsp:include page="includes/footer.jsp"></jsp:include>
</body>
</html>
