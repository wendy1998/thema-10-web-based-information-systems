<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: wendy
  Date: 29-11-2018
  Time: 10:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="includes/head.jsp">
        <jsp:param name="pageTitle" value="sign up"></jsp:param>
    </jsp:include>
    <script src="js/toggle_visibility.js"></script>
</head>
<body>
<jsp:include page="includes/logo.jsp"></jsp:include>
<div class="row inhoud">
    <div class="col">

    </div>
    <div class="col">
        <h1>Maak (team)account</h1>
        <p>Heb je al een account? Klik dan <a href="index.jsp">hier</a></p>
        <c:if test="${requestScope.succesfullSignup != null}">
            <p class="green">${requestScope.succesfullSignup}</p>
        </c:if>
        <form action="signup.do" method="post">
            <label>Login naam&nbsp;&nbsp;</label><input type="text" name="username"/><br/>
            <c:if test="${requestScope.usernameError != null}">
                <p class="red">${requestScope.usernameError}</p>
            </c:if>
            <label>Wachtwoord&nbsp;&nbsp;</label><input type="password" name="password1"/><br/>
            <label>Herhaal wachtwoord&nbsp;&nbsp;</label><input type="password" name="password2"/><br/>
            <c:if test="${requestScope.passwordError != null}">
                <p class="red">${requestScope.passwordError}</p>
            </c:if>
            <label>Ik ben een docent <input type="checkbox" id="docent_check" name="docent_check"/></label><br/>
            <label class="hidden" id="docent_code">Docent code&nbsp;&nbsp;<input type="password" name="docent_code"></label><br/>
            <c:if test="${requestScope.teacherError != null}">
                <p class="red">${requestScope.teacherError}</p>
            </c:if>
            <input type="submit" name="submit" value="submit" id="submit"/>
        </form>
    </div>
    <div class="col">

    </div>
</div>
<script type="text/javascript">
    if(document.readyState) {
        window.setInterval(function () {
            check_docent_code();
        }, 10);
    }
</script>
<jsp:include page="includes/footer.jsp"></jsp:include>
</body>
</html>
