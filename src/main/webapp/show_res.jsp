<%--
  Created by IntelliJ IDEA.
  User: wbvandermeulen
  Date: 4-12-18
  Time: 14:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <jsp:include page="includes/head.jsp">
        <jsp:param name="pageTitle" value="resultaat bekijken"></jsp:param>
    </jsp:include>
    <script src="js/fill_table.js"></script>
    <script src="js/show_diffen.js"></script>
</head>
<body>
    <jsp:include page="includes/header.jsp"></jsp:include>
    <div class="inhoud">
        <div class="container">
            <table class="table table-bordered table-hover table-striped" id="datatable" width="100%">
                <thead>
                    <tr>
                        <th>team</th>
                        <th>analist</th>
                        <th>sample label</th>
                        <th>sample type</th>
                        <th>patient num</th>
                        <th>dag ingevoerd</th>
                        <th>tijd ingevoerd</th>
                        <th>creatinine urine per dag</th>
                        <th>creatinine urine per liter</th>
                        <th>creatinine bloed</th>
                        <th>ureum per dag</th>
                        <th>ureum per liter</th>
                        <th>hematocriet</th>
                        <th>hemoglobine</th>
                        <th>diffen</th>
                        <th>salicylzuur</th>
                        <th>LDH</th>
                        <th>MCV</th>
                        <th>MCH</th>
                        <th>MCHC</th>
                        <th>note</th>
                        <th>revision?</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>bla</td>
                        <td>bla1</td>
                        <td>bla2</td>
                        <td>bla3</td>
                        <td>bla4</td>
                        <td>bla5</td>
                        <td>bla6</td>
                        <td>bla7</td>
                        <td>bla8</td>
                        <td>bla9</td>
                        <td>bla9</td>
                        <td>bla9</td>
                        <td>bla9</td>
                        <td>bla9</td>
                        <td>bla9</td>
                        <td>bla9</td>
                        <td>bla9</td>
                        <td>bla9</td>
                        <td>bla9</td>
                        <td>bla9</td>
                        <td>bla9</td>
                        <td>bla9</td>
                    </tr>
                </tbody>
            </table>
            <div class="row" id="dataexport">
                <div class="col text-center">
                    <h3>Export data</h3>
                    <c:if test="${requestScope.errorMessage != null}">
                        <p class="red">${requestScope.errorMessage}</p>
                    </c:if>
                    <c:if test="${requestScope.successMessage != null}">
                        <p class="green">${requestScope.successMessage}</p>
                    </c:if>
                    <form action="datadownload" method="post">
                        <label>Output Filenaam:&nbsp;&nbsp;<input type="text" name="outfile" required/></label><br/>
                        <label>Output type:&nbsp;&nbsp;<select name="outtype">
                            <option>Alles</option>
                            <option>Alleen vandaag</option>
                        </select></label><br/>
                        <input type="submit" id="submit" name="submit" value="Exporteren">
                    </form>
                </div>
                <div class="col">

                </div>
                <div class="col">

                </div>
            </div>
        </div>
    </div>
    <c:choose>
        <c:when test="${sessionScope.user.role == 'TEACHER'}">
            <script type="text/javascript">
                if(document.readyState) {
                    var table = $('#datatable').DataTable({
                        scrollX: 200,
                        rowGroup: {
                            dataSrc: 0
                        }
                    });
                    get_data(table);
                    show_diffen(table);

                    window.setInterval(function () {
                        get_data(table);
                    }, 5000*60000);
                }
            </script>
        </c:when>
        <c:otherwise>
            <script type="text/javascript">
                if(document.readyState) {
                    var table = $('#datatable').DataTable({
                        scrollX: 200
                    });
                    get_data(table);
                    show_diffen(table);
                }
            </script>
        </c:otherwise>
    </c:choose>
    <jsp:include page="includes/footer.jsp"></jsp:include>
</body>
</html>