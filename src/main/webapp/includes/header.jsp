<%--
  Created by IntelliJ IDEA.
  User: wbvandermeulen
  Date: 12-12-18
  Time: 9:19
  To change this template use File | Settings | File Templates.
--%>
<div class="row header">
    <div class="col-3 logo">
        <p class="align-bottom">HANZE HUISARTSENLABORATORIUM<br/>
            Voor clinisch chemisch onderzoek</p>
    </div>
    <div class="col-3 menu">
        <div class="row">
            <div class="col">
                <a href="res_invoer.jsp">Resultaat invoer</a>
            </div>
            <div class="col">
                <a href="show_res.jsp">Resultaten bekijken</a>
            </div>
        </div>
    </div>
</div>
