var fill_table = function(table, data) {
    table.clear().draw();
    table.rows.add(data);
    // Adjust column width depending on new contents (making sure it fits)
    table.columns.adjust().draw();
};

/**
 * Retrieves data using Jquery AJAX calls (getJSON function).
 * Only when both calls are finished, the data is processed
 */
var get_data = function(table) {
    $.when(
        // Request data
        $.getJSON("overview", function (data) {
            results = data
        })
    ).then(function () {
        fill_table(table, results);
    });
};