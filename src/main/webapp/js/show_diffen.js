function format (diffenId, diffenValues) {
    if(diffenId == "null") {
        return "Geen diffen beschikbaar"
    } else {
        var diffenTableList = ['<table class="table-bordered table-responsive">' +
        '<thead>' +
        '<tr>' +
        '<th>myeloblasten</th>' +
        '<th>promyeloblasten</th>' +
        '<th>myelocyten</th>' +
        '<th>metamyelocyten</th>' +
        '<th>neutrofiele granulocyten</th>' +
        '<th>eosinofielen</th>' +
        '<th>basofielen</th>' +
        '<th>monocyten</th>' +
        '<th>lymfocyten</th>' +
        '<th>plasmacellen</th>' +
        '<th>erytroblasten</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>' +
        '<tr>'];

        for(var i=0; i < diffenValues.length; i++) {
            diffenTableList.push('<td>', diffenValues[i], '</td>')
        }

        diffenTableList.push(
            '</tr>' +
            '</tbody>' +
            '</table>'
        );
        return diffenTableList.join("");
    }
}

function show_diffen(table) {
    // Add event listener for opening and closing details
    $('#datatable tbody').on('click', 'tr', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
        var diffenId = tr.context.cells[14].firstChild.textContent;

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            if(diffenId != "null") {
                var resultsDiffen;
                $.when(
                    // Request data
                    $.getJSON("show_diffen", {diffenId: diffenId}, function (data) {
                        resultsDiffen = data;
                    })
                ).then( function () {
                    // Open this row
                    row.child(format(diffenId, resultsDiffen)).show();
                    tr.addClass('shown');
                });
            } else {
                // Open this row
                row.child(format(diffenId)).show();
                tr.addClass('shown');
            }
        }
    } );
}