// creatinine
function check_creatinine() {
    var cr_ur_lit_check = document.getElementById("cr_ur_lit_check");
    var cr_ur_day_check = document.getElementById("cr_ur_dag_check");
    var cr_uri_txt = document.getElementById("cr_uri_txt");
    var cr_div = document.getElementById("cr_div");
    var cr_ur_lit_txt = document.getElementById("cr_ur_lit_txt");
    var cr_ur_dag_txt = document.getElementById("cr_ur_dag_txt");
    var cr_bl_check = document.getElementById("cr_bl_check");
    var cr_bl_txt = document.getElementById("cr_bloed_txt");

    if (cr_ur_lit_check.checked || cr_ur_day_check.checked || cr_bl_check.checked) {
        if (cr_div.classList.contains("hidden")) {
            cr_div.classList.replace("hidden", "show");
        }

        if (cr_ur_lit_check.checked || cr_ur_day_check.checked) {
            if (cr_uri_txt.classList.contains("hidden")) {
                cr_uri_txt.classList.replace("hidden", "show");
            }

            if (cr_ur_lit_check.checked) {
                if (cr_ur_lit_txt.classList.contains("hidden")) {
                    cr_ur_lit_txt.classList.replace("hidden", "show");
                }
            } else {
                if (cr_ur_lit_txt.classList.contains("show")) {
                    cr_ur_lit_txt.classList.replace("show", "hidden");
                }
            }

            if (cr_ur_day_check.checked) {
                if (cr_ur_dag_txt.classList.contains("hidden")) {
                    cr_ur_dag_txt.classList.replace("hidden", "show");
                }
            } else {
                if (cr_ur_dag_txt.classList.contains("show")) {
                    cr_ur_dag_txt.classList.replace("show", "hidden");
                }
            }
        } else {
            if (cr_ur_dag_txt.classList.contains("show")) {
                cr_ur_dag_txt.classList.replace("show", "hidden");
            }
            if (cr_ur_lit_txt.classList.contains("show")) {
                cr_ur_lit_txt.classList.replace("show", "hidden");
            }
            if (cr_uri_txt.classList.contains("show")) {
                cr_uri_txt.classList.replace("show", "hidden");
            }
        }

        if(cr_bl_check.checked) {
            if (cr_bl_txt.classList.contains("hidden")) {
                cr_bl_txt.classList.replace("hidden", "show");
            }
        } else {
            if (cr_bl_txt.classList.contains("show")) {
                cr_bl_txt.classList.replace("show", "hidden");
            }
        }
    } else {
        if (cr_div.classList.contains("show")) {
            cr_div.classList.replace("show", "hidden");
        }
        if (cr_ur_dag_txt.classList.contains("show")) {
            cr_ur_dag_txt.classList.replace("show", "hidden");
        }
        if (cr_ur_lit_txt.classList.contains("show")) {
            cr_ur_lit_txt.classList.replace("show", "hidden");
        }
        if (cr_uri_txt.classList.contains("show")) {
            cr_uri_txt.classList.replace("show", "hidden");
            cr_uri_txt.classList.add("hidden");
        }
        if (cr_bl_txt.classList.contains("show")) {
            cr_bl_txt.classList.replace("show", "hidden");
        }
    }
}

// diffen
function check_diffen() {
    var diffen_check = document.getElementById("diffen_check");
    var diffen_txt = document.getElementById("diffen");

    if(diffen_check.checked) {
        if (diffen_txt.classList.contains("hidden")) {
            diffen_txt.classList.replace("hidden", "show");
        }
    } else {
        if(diffen_txt.classList.contains("show")) {
            diffen_txt.classList.replace("show", "hidden");
        }
    }
}

// ureum
function check_ureum() {
    var ur_lit_check = document.getElementById("ur_lit_check");
    var ur_dag_check = document.getElementById("ur_dag_check");
    var ur_dag = document.getElementById("ur_dag");
    var ur_lit = document.getElementById("ur_lit");
    var ureum_div = document.getElementById("ureum_div");

    if(ur_lit_check.checked || ur_dag_check.checked) {
        if(ureum_div.classList.contains("hidden")) {
            ureum_div.classList.replace("hidden", "show");
        }
        if(ur_dag_check.checked) {
            if(ur_dag.classList.contains("hidden")) {
                ur_dag.classList.replace("hidden", "show");
            }
        }

        if(ur_lit_check) {
            if(ur_lit.classList.contains("hidden")) {
                ur_lit.classList.replace("hidden", "show");
            }
        }
    } else {
        if (ureum_div.classList.contains("show")) {
            ureum_div.classList.replace("show", "hidden");
        }
    }
    if(!ur_lit_check.checked) {
        if (ur_lit.classList.contains("show")) {
            ur_lit.classList.replace("show", "hidden");
        }
    }
    if(!ur_dag_check.checked) {
        if (ur_dag.classList.contains("show")) {
            ur_dag.classList.replace("show", "hidden");
        }
    }
}

// others
function check_others() {
    var overig_checks = document.getElementsByClassName("overig");
    var hematocriet_check = document.getElementById("hematocriet_check");
    var hemoglobine_check = document.getElementById("hemoglobine_check");
    var salicylzuur_check = document.getElementById("salicylzuur_check");
    var ldh_check = document.getElementById("LDH_check");
    var mcv_check = document.getElementById("MCV_check");
    var mch_check = document.getElementById("MCH_check");
    var mchc_check = document.getElementById("MCHC_check");

    var overig_div = document.getElementById("overig_div");
    var hematocriet = document.getElementById("Hematocriet");
    var hemoglobine = document.getElementById("Hemoglobine");
    var salicylzuur = document.getElementById("Salicylzuur");
    var ldh = document.getElementById("LDH");
    var mcv = document.getElementById("MCV");
    var mch = document.getElementById("MCH");
    var mchc = document.getElementById("MCHC");

    var isChecked = false;
    for(var i=0; i < overig_checks.length; i++) {
        if(overig_checks.item(i).checked) {
            isChecked = true;
            break;
        }
    }

    if(isChecked) {
        if(overig_div.classList.contains("hidden")) {
            overig_div.classList.replace("hidden", "show");
        }
        if(hematocriet_check.checked) {
            if (hematocriet.classList.contains("hidden")) {
                hematocriet.classList.replace("hidden", "show");
            }
        }
        if(hemoglobine_check.checked) {
            if(hemoglobine.classList.contains("hidden")) {
                hemoglobine.classList.replace("hidden", "show");
            }
        }
        if(salicylzuur_check.checked) {
            if(salicylzuur.classList.contains("hidden")) {
                salicylzuur.classList.replace("hidden", "show");
            }
        }
        if(ldh_check.checked) {
            if(ldh.classList.contains("hidden")) {
                ldh.classList.replace("hidden", "show");
            }
        }
        if(mcv_check.checked) {
            if(mcv.classList.contains("hidden")) {
                mcv.classList.replace("hidden", "show");
            }
        }
        if(mch_check.checked) {
            if(mch.classList.contains("hidden")) {
                mch.classList.replace("hidden", "show");
            }
        }
        if(mchc_check.checked) {
            if(mchc.classList.contains("hidden")) {
                mchc.classList.replace("hidden", "show");
            }
        }
    } else {
        if(overig_div.classList.contains("show")) {
            overig_div.classList.replace("show", "hidden");
        }
    }
    if(!hematocriet_check.checked) {
        if (hematocriet.classList.contains("show")) {
            hematocriet.classList.replace("show", "hidden");
        }
    }
    if(!hemoglobine_check.checked) {
        if(hemoglobine.classList.contains("show")) {
            hemoglobine.classList.replace("show", "hidden");
        }
    }
    if(!salicylzuur_check.checked) {
        if(salicylzuur.classList.contains("show")) {
            salicylzuur.classList.replace("show", "hidden");
        }
    }
    if(!ldh_check.checked) {
        if(ldh.classList.contains("show")) {
            ldh.classList.replace("show", "hidden");
        }
    }
    if(!mcv_check.checked) {
        if(mcv.classList.contains("show")) {
            mcv.classList.replace("show", "hidden");
        }
    }
    if(!mch_check.checked) {
        if(mch.classList.contains("show")) {
            mch.classList.replace("show", "hidden");
        }
    }
    if(!mchc_check.checked) {
        if(mchc.classList.contains("show")) {
            mchc.classList.replace("show", "hidden");
        }
    }
}

// docent code sign up
function check_docent_code() {
    var docent_check = document.getElementById("docent_check");
    var docent_code = document.getElementById("docent_code");

    if(docent_check.checked) {
        if(docent_code.classList.contains("hidden")) {
            docent_code.classList.replace("hidden", "show");
        }
    } else {
        if(docent_code.classList.contains("show")) {
            docent_code.classList.replace("show", "hidden");
        }
    }
}