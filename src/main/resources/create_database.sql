drop table if exists EXPERIMENTS;
drop table if exists USERS;
drop table if exists DIFFEN;
drop table if exists SAMPLES;
drop table if exists PATIENTS;

create table PATIENTS (
  patient_num int not null unique,
  name varchar(500),
  date_of_birth varchar(200) not null,
  primary key(patient_num)
);

create table SAMPLES (
  label int not null unique,
  date_aquired varchar(250) not null,
  time_aquired varchar(250) null,
  type varchar(250) not null,
  patient int not null,
  primary key(label),
  foreign key(patient)
    references PATIENTS(patient_num)
);

create table DIFFEN (
  id int not null auto_increment,
  myeloblasten double not null,
  promyeloblasten double not null,
  myelocyten double not null,
  metamyelocyten double not null,
  neutrofiele_granulocyten double not null,
  eosinofielen double not null,
  basofielen double not null,
  monocyten double not null,
  lymfocyten double not null,
  plasmacellen double not null,
  erytroblasten double not null,
  primary key(id)
);

create table USERS (
  id int not null auto_increment,
  username varchar(500) not null,
  password varchar(500) not null,
  role varchar(500) not null,
  primary key(id)
);

create table EXPERIMENTS (
  id int not null auto_increment,
  team int not null,
  added_by varchar(500) not null,
  date_result varchar(150) not null,
  time_result varchar(150) not null,
  sample int not null,
  creatinine_urine_per_liter double null,
  creatinine_urine_per_day double null,
  creatinine_blood double null,
  hematocriet double null,
  hemaglobine double null,
  diffen int null,
  salicylzuur double null,
  ldh double null,
  ureum_per_liter double null,
  ureum_per_day double null,
  mvc double null,
  mch double null,
  mchc double null,
  note varchar(2000) null,
  revision boolean not null default false,
  primary key(id),
  foreign key(sample)
    references SAMPLES(label),
  foreign key(diffen)
    references DIFFEN(id),
  foreign key(team)
    references USERS(id)
);

insert into USERS(username, password, role) values ('test_team', 'team', 'TEAM');
insert into USERS(username, password, role) values ('test_teacher', 'teacher', 'TEACHER');