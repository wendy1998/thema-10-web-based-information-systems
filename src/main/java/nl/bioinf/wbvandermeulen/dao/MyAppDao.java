package nl.bioinf.wbvandermeulen.dao;

import nl.bioinf.wbvandermeulen.experiment_classes.Experiment;
import nl.bioinf.wbvandermeulen.experiment_classes.Patient;
import nl.bioinf.wbvandermeulen.experiment_classes.Sample;
import nl.bioinf.wbvandermeulen.users.Role;
import nl.bioinf.wbvandermeulen.users.User;

import java.io.IOException;
import java.util.List;

public interface MyAppDao{
    /**
     * connects to the data layer.
     * @throws DatabaseException
     */
    void connect() throws DatabaseException;

    /**
     * fetches a user by username and password.
     * @param userName
     * @param userPass
     * @return
     * @throws DatabaseException
     */
    User getUser(String userName, String userPass) throws DatabaseException;

    /**
     * fetches a user id by username and password.
     * @param team user of which id needs to be fetched
     * @return
     * @throws DatabaseException
     */
    int getUserId(User team) throws DatabaseException;

    /**
     * get a sample by it's label
     * @param label the label of the sample to get
     * @return sample object
     */
    Sample getSample(int label);

    /**
     * @return all the passwords already in use
     * @throws DatabaseException
     */
    List<String> getPasswords() throws DatabaseException;

    /**
     *
     * @return al the usernames already in use
     * @throws DatabaseException
     */
    List<String> getUsernames() throws DatabaseException;
    /**
     * inserts a new User.
     * @param userName
     * @param userPass
     * @param role
     * @throws DatabaseException
     */
    void insertUser(String userName, String userPass, Role role) throws DatabaseException;

    /**
     * Parameters are all the fields that need to be filled in the database
     * @throws DatabaseException when something goes wrong while inserting the data
     */
    void insertSample(Sample sample) throws DatabaseException;

    /**
     * Gets the patient belonging to a specific id
     * @param patient_num the id of the patient
     * @return the result of the query
     */
    Patient getPatient(int patient_num);

    /**
     * Parameters are all the fields that need to be filled in the database
     * @throws DatabaseException when something goes wrong while inserting the data
     */
    void insertPatient(Patient patient) throws DatabaseException;

    /**
     * Parameters are all the fields that need to be filled in the database
     * @throws DatabaseException when something goes wrong while inserting the data
     */
    void insertExperiment(Experiment experiment) throws DatabaseException;

    /**
     * Gets the values of the diffen table with that specific id
     * @param diffenId
     * @return diffemValues, the values in the table
     */
    List<Double> getDiffenValues(int diffenId) throws DatabaseException;

    /**
     * Gets the values of the diffen table with that specific id in json format
     * @param diffenId
     * @return diffemValues, the values in the table
     */
    String getDiffenValuesJSON(int diffenId) throws DatabaseException;

    /**
     * get's an overview of all the experiments on the name of a specific team or all of them.
     * @param signedInUser
     * @return
     */
    List<List<String>> getExperimentalOverview(User signedInUser) throws DatabaseException;

    int getNewDiffenId();

    /**
     * Gets the id of the diffen table with those specific elements
     * @param diffenValues
     * @return
     * @throws DatabaseException
     */
    int getDiffenId(List<Double> diffenValues);

    /**
     * Parameter is all the fields that need to be filled in the database
     * @throws DatabaseException when something goes wrong while inserting the data
     */
    void insertDiffen(List<Double> diffenValues) throws DatabaseException;

    /**
     * Exports the data of a specific user or all users to a file in a certain way
     * @param fileName the file the data has to be written to
     * @param signedInUser the user that's signed in
     * @param type all the data or only of today
     * @throws IOException when some operation doesn't work
     */
    void exportData(String fileName, User signedInUser, String type) throws IOException, DatabaseException;

    /**
     * closes the connection to the data layer and frees resources.
     */
    void disconnect();
}