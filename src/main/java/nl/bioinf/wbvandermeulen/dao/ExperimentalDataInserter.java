package nl.bioinf.wbvandermeulen.dao;

import nl.bioinf.wbvandermeulen.experiment_classes.Experiment;
import nl.bioinf.wbvandermeulen.experiment_classes.Patient;
import nl.bioinf.wbvandermeulen.experiment_classes.Sample;
import nl.bioinf.wbvandermeulen.form_checks.experiment.Diffen;
import nl.bioinf.wbvandermeulen.form_checks.experiment.ExperimentalDataChecker;
import nl.bioinf.wbvandermeulen.users.User;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ExperimentalDataInserter {
    private final Map<String, String> attributes;
    private static final List<String> SAMPLE_ATTRIBUTES = Arrays.asList("slabel", "stype", "sdatbinnen", "stijdbinnen");
    private static final List<String> PATIENT_ATTRIBUTES = Arrays.asList("pnum", "pnaam", "pgebdatum");
    private static final List<String> EXPERIMENT_ATTRIBUTES = Arrays.asList("NA", "NA", "NA", "NA", "NA",
            "cr_ur_lit", "cr_ur_dag", "cr_bloed", "ur_lit", "ur_dag", "Hematocriet", "Hemoglobine", "NA", "Salicylzuur",
            "LDH", "MCV", "MCH", "MCHC", "NA");
    private final MyAppDao dao = MyAppDaoMySQL.getInstance();
    private LocalDate today;
    private LocalTime timeResult;
    private User team;

    public ExperimentalDataInserter(Map<String, String> attributes, LocalDate today, LocalTime time_result, User team) {
        this.attributes = attributes;
        try {
            dao.connect();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        this.today = today;
        this.timeResult = time_result;
        this.team = team;
    }

    public void insertData(List<Object> params) throws DatabaseException {
        int patientIdx = 0;
        int sampleIdx = 1;
        int diffenIdx = 2;
        int experimentIdx = 4;

        Patient patient = (Patient) params.get(patientIdx);
        Sample sample = (Sample) params.get(sampleIdx);
        List<Double> diffen = (List<Double>) params.get(diffenIdx);
        boolean exists = (boolean) params.get(3);
        Experiment experiment = (Experiment) params.get(experimentIdx);

        try {
            if(patient != null) {
                dao.insertPatient(patient);
                System.out.println("inserted 1 patient");
            }
            if(sample != null) {
                dao.insertSample(sample);
                System.out.println("inserted 1 sample");
            }
            if(!exists) {
                dao.insertDiffen(diffen);
                System.out.println("inserted 1 diffen");
            }
            dao.insertExperiment(experiment);
            System.out.println("inserted 1 experiment");
            dao.disconnect();
        } catch (DatabaseException e) {
            e.printStackTrace();
            dao.disconnect();
            throw new DatabaseException("Er gaat iets fout bij het invoeren in de database, probeer het opnieuw.");
        }
    }

    public List<Object> prepareDataInsert() {
        Patient patient = PrepareToInsertPatient();
        Sample sample = PrepareToInsertSample();
        int diffenNum = 0;
        List<Double> diffen = null;
        boolean exists = false;
        if(this.attributes.containsKey("difErytroblasten")) {
            diffen = Diffen.getDiffenValues(attributes);
            diffenNum = dao.getDiffenId(diffen);
            if(diffenNum != 0) {
                exists = true;
            } else {
                diffenNum = this.dao.getNewDiffenId();
            }
        }
        Experiment experiment = PrepareToInsertExperiment(diffenNum);
        return Arrays.asList(patient, sample, diffen, exists, experiment);
    }

    private Patient PrepareToInsertPatient() {
        int pnum = Integer.parseInt(attributes.get(PATIENT_ATTRIBUTES.get(0)));
        String name = attributes.getOrDefault(PATIENT_ATTRIBUTES.get(1), null);
        String gebDatum = attributes.get(PATIENT_ATTRIBUTES.get(2));
        Patient patientFromDatabase = dao.getPatient(pnum);
        if(patientFromDatabase == Patient.NA) {
            return new Patient(pnum, name, gebDatum);
        }
        return null;
    }

    private Sample PrepareToInsertSample() {
        int pnum = Integer.parseInt(attributes.get(PATIENT_ATTRIBUTES.get(0)));
        int label = Integer.parseInt(attributes.get(SAMPLE_ATTRIBUTES.get(0)));
        String type = attributes.get(SAMPLE_ATTRIBUTES.get(1));
        String time = attributes.getOrDefault(SAMPLE_ATTRIBUTES.get(3), null);
        String date = attributes.get(SAMPLE_ATTRIBUTES.get(2));
        Sample sampleFromDatabase = dao.getSample(label);
        if(sampleFromDatabase == Sample.NA) {
            return new Sample(label, type, date, time, pnum);
        }
        return null;
    }

    private Experiment PrepareToInsertExperiment(int diffenNum) {
        int sampleLabel = Integer.parseInt(attributes.get(SAMPLE_ATTRIBUTES.get(0)));
        String todayString = today.format(DateTimeFormatter.ISO_DATE);
        String timeResultString = timeResult.format(DateTimeFormatter.ISO_TIME);
        List<Double> values = new ArrayList<>(19);
        for(int i=0; i < EXPERIMENT_ATTRIBUTES.size(); i++) {
            if(EXPERIMENT_ATTRIBUTES.get(i).equals("NA")) {
                continue;
            } else {
                if(attributes.containsKey(EXPERIMENT_ATTRIBUTES.get(i))) {
                    values.add(Double.parseDouble(attributes.get(EXPERIMENT_ATTRIBUTES.get(i))));
                } else {
                    values.add(0.0);
                }
            }
        }
        boolean revision = attributes.containsKey("revision");
        return new Experiment(team, attributes.get("analist"), todayString,
                timeResultString, sampleLabel, values.get(0), values.get(1),
                values.get(2), values.get(3), values.get(4), values.get(5),
                values.get(6), diffenNum, values.get(7), values.get(8),
                values.get(9), values.get(10), values.get(11), attributes.get("note"),
                revision);
    }
}
