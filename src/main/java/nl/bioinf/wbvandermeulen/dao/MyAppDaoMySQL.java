package nl.bioinf.wbvandermeulen.dao;

import com.google.gson.Gson;
import nl.bioinf.noback.db_utils.DbCredentials;
import nl.bioinf.noback.db_utils.DbUser;
import nl.bioinf.wbvandermeulen.experiment_classes.Experiment;
import nl.bioinf.wbvandermeulen.experiment_classes.Patient;
import nl.bioinf.wbvandermeulen.experiment_classes.Sample;
import nl.bioinf.wbvandermeulen.users.Role;
import nl.bioinf.wbvandermeulen.users.User;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public final class MyAppDaoMySQL implements MyAppDao {
    private static final String GET_USER = "get_user";
    private static final String GET_PASSWORDS = "get_passwords";
    private static final String GET_USERNAMES = "get_usernames";
    private static final String INSERT_USER = "insert_user";
    private static final String INSERT_SAMPLE = "insert_sample";
    private static final String GET_SAMPLE = "get_sample";
    private static final String INSERT_PATIENT = "insert_patient";
    private static final String GET_PATIENT = "get_patient";
    private static final String INSERT_DIFFEN = "insert_diffen";
    private static final String GET_DIFFEN_ID = "get_diffen_id";
    private static final String GET_NEXT_DIFFEN_ID = "get_next_diffen_id";
    private static final String GET_DIFFEN_VALUES = "get_diffen_values";
    private static final String INSERT_EXPERIMENT = "insert_experiment";
    private static final String GET_OVERVIEW_SPECIFIC = "get_overview_specific";
    private static final String GET_OVERVIEW_ALL = "get_overview_all";

    Connection connection;
    private Map<String, PreparedStatement> preparedStatements = new HashMap<>();

    /*singleton pattern*/
    private static MyAppDaoMySQL uniqueInstance;

    /**
     * singleton pattern
     */
    private MyAppDaoMySQL() {}

    /**
     * singleton pattern
     */
    public static MyAppDaoMySQL getInstance() {
        //lazy
        if (uniqueInstance == null) {
            uniqueInstance = new MyAppDaoMySQL();
        }
        return uniqueInstance;
    }

    @Override
    public void connect() throws DatabaseException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            DbUser mySQLuser = DbCredentials.getMySQLuser();
            String dbUrl = "jdbc:mysql://" + mySQLuser.getHost() + "/" + mySQLuser.getDatabaseName();
            String dbUser = mySQLuser.getUserName();
            String dbPass = mySQLuser.getDatabasePassword();
            connection = DriverManager.getConnection(dbUrl,dbUser,dbPass);
            prepareStatements();
        } catch (Exception e) {
            e.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    e.getCause());
        }
    }

    /**
     * prepares prepared statements for reuse
     * @throws SQLException when statements can't be prepared
     */
    private void prepareStatements() throws SQLException {
        String fetchUserQuery = "SELECT * FROM USERS WHERE username = ? AND password = ?";
        PreparedStatement ps = connection.prepareStatement(fetchUserQuery);
        this.preparedStatements.put(GET_USER, ps);

        String fetchPasswordsQuery = "SELECT password FROM USERS";
        ps = connection.prepareStatement(fetchPasswordsQuery);
        this.preparedStatements.put(GET_PASSWORDS, ps);

        String fetchUsernamesQuery = "SELECT username FROM USERS";
        ps = connection.prepareStatement(fetchUsernamesQuery);
        this.preparedStatements.put(GET_USERNAMES, ps);

        String insertUserQuery = "INSERT INTO USERS (username, password, role) "
                        + " VALUES (?, ?, ?)";
        ps = connection.prepareStatement(insertUserQuery);
        this.preparedStatements.put(INSERT_USER, ps);

        String insertSampleQuery = "INSERT INTO SAMPLES(label, date_aquired, time_aquired, type, patient)"
                + " VALUES (?, ?, ?, ?, ?)";
        ps = connection.prepareStatement(insertSampleQuery);
        this.preparedStatements.put(INSERT_SAMPLE, ps);

        String insertPatientQuery = "insert into PATIENTS(patient_num, name, date_of_birth) " +
                "values (?, ?, ?);";
        ps = connection.prepareStatement(insertPatientQuery);
        this.preparedStatements.put(INSERT_PATIENT, ps);

        String insertDiffenQuery = "insert into DIFFEN(myeloblasten, promyeloblasten, myelocyten, metamyelocyten," +
                " neutrofiele_granulocyten, eosinofielen, basofielen, monocyten, lymfocyten, plasmacellen," +
                "  erytroblasten) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        ps = connection.prepareStatement(insertDiffenQuery);
        this.preparedStatements.put(INSERT_DIFFEN, ps);

        String insertExperimentQuery = "insert into EXPERIMENTS(team, added_by, date_result, time_result," +
                " sample, creatinine_urine_per_liter, creatinine_urine_per_day, creatinine_blood," +
                " ureum_per_liter, ureum_per_day, hematocriet, hemaglobine," +
                " diffen, salicylzuur, ldh, mvc, mch, " +
                " mchc, note, revision) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        ps = connection.prepareStatement(insertExperimentQuery);
        this.preparedStatements.put(INSERT_EXPERIMENT, ps);

        String getDiffenIdQuery = "SELECT id FROM DIFFEN WHERE myeloblasten = ? and promyeloblasten = ? and myelocyten = ? " +
                "and metamyelocyten = ? and neutrofiele_granulocyten = ? and eosinofielen = ? and basofielen = ? " +
                "and monocyten = ? and lymfocyten = ? and plasmacellen = ? and erytroblasten = ?";
        ps = connection.prepareStatement(getDiffenIdQuery);
        this.preparedStatements.put(GET_DIFFEN_ID, ps);

        String getNextDiffenIdQuery = "SELECT AUTO_INCREMENT FROM information_schema.TABLES " +
                "WHERE TABLE_NAME = \"DIFFEN\";";
        ps = connection.prepareStatement(getNextDiffenIdQuery);
        this.preparedStatements.put(GET_NEXT_DIFFEN_ID, ps);

        String getDiffenValuesQuery = "SELECT myeloblasten, promyeloblasten, myelocyten, " +
                "metamyelocyten, neutrofiele_granulocyten, eosinofielen, basofielen, " +
                "monocyten, lymfocyten, plasmacellen, erytroblasten from DIFFEN where id = ?";
        ps = connection.prepareStatement(getDiffenValuesQuery);
        this.preparedStatements.put(GET_DIFFEN_VALUES, ps);

        String getPatientQuery = "SELECT * FROM PATIENTS WHERE patient_num = ?";
        ps = connection.prepareStatement(getPatientQuery);
        this.preparedStatements.put(GET_PATIENT, ps);

        String getSampleQuery = "SELECT * FROM SAMPLES WHERE label = ?";
        ps = connection.prepareStatement(getSampleQuery);
        this.preparedStatements.put(GET_SAMPLE, ps);

        String getOverviewSpecificQuery = "select username, added_by, sample, type, patient, date_result, time_result, " +
                "creatinine_urine_per_day, creatinine_urine_per_liter, " +
                "creatinine_blood, ureum_per_day, ureum_per_liter, hematocriet, " +
                "hemaglobine, diffen, salicylzuur, ldh, mvc, mch, mchc, " +
                "note, revision from USERS u join EXPERIMENTS e on u.id = e.team join " +
                "SAMPLES s on e.sample = s.label where u.username = ?;";
        ps = connection.prepareStatement(getOverviewSpecificQuery);
        this.preparedStatements.put(GET_OVERVIEW_SPECIFIC, ps);

        String getOverviewAllQuery = "select username, added_by, sample, type, patient, date_result, time_result, " +
                "creatinine_urine_per_day, creatinine_urine_per_liter, " +
                "creatinine_blood, ureum_per_day, ureum_per_liter, hematocriet, " +
                "hemaglobine, diffen, salicylzuur, ldh, mvc, mch, mchc, " +
                "note, revision from USERS u join EXPERIMENTS e on u.id = e.team join " +
                "SAMPLES s on e.sample = s.label;";
        ps = connection.prepareStatement(getOverviewAllQuery);
        this.preparedStatements.put(GET_OVERVIEW_ALL, ps);
    }

    @Override
    public List<String> getPasswords() throws DatabaseException {
        try {
            List<String> passwords = new ArrayList<>();
            PreparedStatement ps = this.preparedStatements.get(GET_PASSWORDS);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                passwords.add(rs.getString("password"));
            }
            rs.close();
            return passwords;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    e.getCause());

        }
    }

    @Override
    public List<String> getUsernames() throws DatabaseException {
        try {
            List<String> usernames = new ArrayList<>();
            PreparedStatement ps = this.preparedStatements.get(GET_USERNAMES);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                usernames.add(rs.getString("username"));
            }
            rs.close();
            return usernames;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    e.getCause());

        }
    }

    @Override
    public User getUser(String userName, String userPass) throws DatabaseException  {
        try {
            PreparedStatement ps = this.preparedStatements.get(GET_USER);
            ps.setString(1, userName);
            ps.setString(2, userPass);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String userRoleStr = rs.getString("role");
                Role role = Role.valueOf(userRoleStr);
                User user = new User(userName, userPass, role);
                return user;
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    e.getCause());

        }
        return null;
    }

    @Override
    public int getUserId(User team) throws DatabaseException {
        try {
            PreparedStatement ps = this.preparedStatements.get(GET_USER);
            ps.setString(1, team.getUserName());
            ps.setString(2, team.getPassword());
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                return rs.getInt("id");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    e.getCause());

        }
        return 0;
    }

    @Override
    public void insertUser(String userName, String userPass, Role role) throws DatabaseException  {
        try{
            PreparedStatement ps = this.preparedStatements.get(INSERT_USER);
            ps.setString(1, userName);
            ps.setString(2, userPass);
            ps.setString(3, role.toString());
            ps.executeUpdate();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    ex.getCause());
        }
    }

    @Override
    public Sample getSample(int label) {
        try {
            PreparedStatement ps = this.preparedStatements.get(GET_SAMPLE);
            ps.setInt(1, label);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                int pnum = rs.getInt("patient");
                String date_aquired = rs.getString("date_aquired");
                String time_aquired = rs.getString("time_aquired");
                String type = rs.getString("type");
                return new Sample(label, type, date_aquired, time_aquired, pnum);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return Sample.NA;

        }
        return Sample.NA;
    }

    @Override
    public void insertSample(Sample sample) throws DatabaseException {
        try{
            PreparedStatement ps = this.preparedStatements.get(INSERT_SAMPLE);
            ps.setInt(1, sample.getLabel());
            ps.setString(2, sample.getDateAdded());
            ps.setString(3, sample.getTimeAdded());
            ps.setString(4, sample.getType());
            ps.setInt(5, sample.getPatient());
            ps.executeUpdate();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    ex.getCause());
        }
    }

    @Override
    public Patient getPatient(int patient_num) {
        try {
            PreparedStatement ps = this.preparedStatements.get(GET_PATIENT);
            ps.setInt(1, patient_num);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                String name = rs.getString("name");
                String gebDate = rs.getString("date_of_birth");
                return new Patient(patient_num, name, gebDate);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return Patient.NA;

        }
        return Patient.NA;
    }

    @Override
    public void insertPatient(Patient patient) throws DatabaseException {
        try{
            PreparedStatement ps = this.preparedStatements.get(INSERT_PATIENT);
            ps.setInt(1, patient.getPnum());
            ps.setString(2, patient.getName());
            ps.setString(3, patient.getBirthDate());
            ps.executeUpdate();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    ex.getCause());
        }
    }

    @Override
    public void insertExperiment(Experiment experiment) throws DatabaseException {
        try{
            PreparedStatement ps = this.preparedStatements.get(INSERT_EXPERIMENT);
            List<Double> params = Arrays.asList(0.1, 0.1, 0.1, 0.1, 0.1, experiment.getCr_ur_lit(), experiment.getCr_ur_dag(), experiment.getCr_bloed(),
                    experiment.getUr_lit(), experiment.getUr_dag(), experiment.getHematocriet(), experiment.getHemoglobine(), 0.1, experiment.getSalicylzuur(),
                    experiment.getLdh(), experiment.getMcv(), experiment.getMch(), experiment.getMchc(), 0.1);

            ps.setInt(1, getUserId(experiment.getTeam()));
            ps.setString(2, experiment.getAnalist());
            ps.setString(3, experiment.getDate_result());
            ps.setString(4, experiment.getTime_result());
            ps.setInt(5, experiment.getSample());

            // all experimentally aquired values
            for(int i=0; i < params.size(); i++) {
                double value = params.get(i);
                if(value == 0.1) {
                    continue;
                } else if(value == 0.0) {
                    ps.setNull((i+1), Types.DOUBLE);
                } else {
                    ps.setDouble((i+1), value);
                }
            }

            if(experiment.getDiffen() == 0) {
                ps.setNull(13, Types.INTEGER);
            } else {
                ps.setInt(13, experiment.getDiffen());
            }
            ps.setString(19, experiment.getNote());
            ps.setBoolean(20, experiment.isRevision());
            ps.executeUpdate();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    ex.getCause());
        }

    }

    @Override
    public List<Double> getDiffenValues(int diffenId) throws DatabaseException {
        try {
            PreparedStatement ps = this.preparedStatements.get(GET_DIFFEN_VALUES);
            ps.setInt(1, diffenId);
            ResultSet rs = ps.executeQuery();
            final ResultSetMetaData meta = rs.getMetaData();
            final int columnCount = meta.getColumnCount();
            final List<Double> diffenValues = new ArrayList<>();
            while (rs.next()) {
                for (int column = 1; column <= columnCount; ++column)
                {
                    final Object value = rs.getObject(column);
                    diffenValues.add(Double.valueOf(String.valueOf(value)));
                }
            }
            rs.close();
            return diffenValues;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;

        }
    }

    @Override
    public String getDiffenValuesJSON(int diffenId) throws DatabaseException {
        List<Double> diffenValues = getDiffenValues(diffenId);
        return new Gson().toJson(diffenValues);
    }

    @Override
    public List<List<String>> getExperimentalOverview(User signedInUser) throws DatabaseException {
        List<List<String>> overview = new ArrayList<>();
        Role userRole = signedInUser.getRole();
        if(userRole == Role.TEAM) {
            try {
                PreparedStatement ps = this.preparedStatements.get(GET_OVERVIEW_SPECIFIC);
                ps.setString(1, signedInUser.getUserName());
                ResultSet rs = ps.executeQuery();
                final ResultSetMetaData meta = rs.getMetaData();
                final int columnCount = meta.getColumnCount();
                while (rs.next()) {
                    List<String> rowList = new ArrayList<>();
                    for (int column = 1; column <= columnCount; ++column)
                    {
                        final Object value = rs.getObject(column);
                        rowList.add(String.valueOf(value));
                    }
                    overview.add(rowList);
                }
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
                PreparedStatement ps = this.preparedStatements.get(GET_OVERVIEW_ALL);
                ResultSet rs = ps.executeQuery();
                final ResultSetMetaData meta = rs.getMetaData();
                final int columnCount = meta.getColumnCount();
                while (rs.next()) {
                    List<String> rowList = new ArrayList<>();
                    for (int column = 1; column <= columnCount; ++column)
                    {
                        final Object value = rs.getObject(column);
                        rowList.add(String.valueOf(value));
                    }
                    overview.add(rowList);
                }
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
        return overview;
    }

    @Override
    public int getNewDiffenId() {
        try {
            PreparedStatement ps = this.preparedStatements.get(GET_NEXT_DIFFEN_ID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt("AUTO_INCREMENT");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
        return 0;
    }

    @Override
    public int getDiffenId(List<Double> diffenValues) {
        try {
            PreparedStatement ps = this.preparedStatements.get(GET_DIFFEN_ID);
            for(int i=0; i < diffenValues.size(); i++) {
                ps.setDouble((i+1), diffenValues.get(i));
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt("id");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void insertDiffen(List<Double> diffenValues) throws DatabaseException {
        try{
            PreparedStatement ps = this.preparedStatements.get(INSERT_DIFFEN);
            for(int i=0; i < diffenValues.size(); i++) {
                ps.setDouble((i+1), diffenValues.get(i));
            }
            ps.executeUpdate();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    ex.getCause());
        }

    }

    @Override
    public void exportData(String fileName, User signedInUser, String type) throws IOException, DatabaseException {
        List<String> lines = null;
        Role userRole = signedInUser.getRole();
        if(userRole == Role.TEAM) {
            try {
                PreparedStatement ps = this.preparedStatements.get(GET_OVERVIEW_SPECIFIC);
                ps.setString(1, signedInUser.getUserName());
                ResultSet rs = ps.executeQuery();
                lines = processResultsetDataExport(rs, type);
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        }
        else {
            try {
                PreparedStatement ps = this.preparedStatements.get(GET_OVERVIEW_ALL);
                ResultSet rs = ps.executeQuery();
                lines = processResultsetDataExport(rs, type);
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DatabaseException(e.getMessage());
            }
        }
        Path path = Paths.get(fileName);
        Charset charset = Charset.defaultCharset();
        try (BufferedWriter writer = Files.newBufferedWriter(path, charset)) {
            Boolean noData = false;
            if(lines.size() == 1){
                noData = true;
            }
            for(String line:lines) {
                writer.write(line);
            }
            if(noData) {
                writer.write("Geen data om weer te geven.");
            }
        }
    }

    private List<String> processResultsetDataExport(ResultSet rs, String type) {
        final int sampleIdx = 3;
        final int ignoreIdx = 4;
        final int patientIdx = 5;
        final int resultDateIdx = 6;
        final int diffenIdx = 15;
        List<String> lines = new ArrayList<>();
        lines.add("team;analist;sample label;sample type;" +
                "sample ingevoerd datum;sample ingevoerd tijd;patient num;patient naam;geboorte datum;" +
                "dag ingevoerd;tijd ingevoerd;creatinine urine per dag;creatinine urine per liter;creatinine bloed;" +
                "ureum per dag;ureum per liter;hematocriet;hemoglobine;" +
                "Myeloblasten;Promyeloblasten;Myelocyten;Metamyelocyten;Neutrofiele granulocyten;Eosinofielen;" +
                "Basofielen;Monocyten;Lymfocyten;Plasmacellen;Erytroblasten;Salicylzuur;LDH;MCV;MCH;MCHC;note;revision?\n");
        try {
            final ResultSetMetaData meta = rs.getMetaData();
            final int columnCount = meta.getColumnCount();
            while (rs.next()) {
                List<String> line = new ArrayList<>();
                Boolean goAhead = true;
                if (type.equals("today")) {
                    String value = rs.getString(resultDateIdx);
                    LocalDate parse = LocalDate.parse(value, DateTimeFormatter.ISO_DATE);
                    LocalDate now = LocalDate.now();
                    if (!parse.isEqual(now)) {
                        goAhead = false;
                    }
                }
                if (goAhead) {
                    for (int column = 1; column <= columnCount; ++column) {
                        final Object value = rs.getObject(column);
                        if(column == sampleIdx) {
                            Sample sample = getSample(Integer.valueOf(String.valueOf(value)));
                            line.add(String.valueOf(sample.getLabel()));
                            line.add(String.valueOf(sample.getType()));
                            line.add(String.valueOf(sample.getDateAdded()));
                            if (sample.getTimeAdded() != null) {
                                line.add(String.valueOf(sample.getTimeAdded()));
                            } else {
                                line.add("null");
                            }
                        } else if (column == ignoreIdx) {
                            continue;
                        } else if (column == patientIdx) {
                            Patient patient = getPatient(Integer.valueOf(String.valueOf(value)));
                            line.add(String.valueOf(patient.getPnum()));
                            if (patient.getName() != null) {
                                line.add(patient.getName());
                            } else {
                                line.add("null");
                            }
                            line.add(patient.getBirthDate());
                        } else if (column == diffenIdx) {
                            if (value != null) {
                                List<Double> diffenValues = getDiffenValues(Integer.valueOf(String.valueOf(value)));
                                for (Double val : diffenValues) {
                                    line.add(String.valueOf(val));
                                }
                            } else {
                                for (int i = 0; i < 11; i++) {
                                    line.add("null");
                                }
                            }
                        } else {
                            if (value == null) {
                                line.add("null");
                            } else {
                                String stringVal = String.valueOf(value);
                                line.add(stringVal);
                            }
                        }
                    }
                    lines.add(String.join(";", line));
                    lines.add("\n");
                }
            }
            rs.close();
        } catch (SQLException | DatabaseException e) {
            e.printStackTrace();
        }
        return lines;
    }

    @Override
    public void disconnect() {
        try{
            for( String key : this.preparedStatements.keySet() ){
                this.preparedStatements.get(key).close();
            }
        } catch( Exception e ){
            e.printStackTrace();
        }
        finally{
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
