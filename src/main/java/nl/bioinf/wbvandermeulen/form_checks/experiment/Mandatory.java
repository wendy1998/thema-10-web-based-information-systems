package nl.bioinf.wbvandermeulen.form_checks.experiment;

public enum Mandatory {
    RESEARCHER("analist"),
    SLABEL("slabel"),
    SDAT("sdatbinnen"),
    PNUM("pnum"),
    PGEBDATUM("pgebdatum"),
    STYPE("stype");

    public String name;

    Mandatory(String name) {
        this.name = name;
    }
}
