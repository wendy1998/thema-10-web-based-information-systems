package nl.bioinf.wbvandermeulen.form_checks.experiment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public enum Diffen {
    difMyeloblasten,
    difMyelocyten,
    difPromyeloblasten,
    difMetamyelocyten,
    difNeutrofiele_granulocyten,
    difEosinofielen,
    difMonocyten,
    difPlasmacellen,
    difBasofielen,
    difLymfocyten,
    difErytroblasten;

    public static List<String> getDiffenAttributes() {
        List<String> attributes = new ArrayList<>(11);
        for(Diffen attr: Diffen.values()) {
            attributes.add(attr.name());
        }
        return attributes;
    }

    public static List<Double> getDiffenValues(Map<String, String> attributes) {
        List<String> diffenAttributes = Diffen.getDiffenAttributes();
        List<Double> diffenValues = new ArrayList<>(11);
        for(String id: diffenAttributes) {
            double value = Double.parseDouble(attributes.get(id));
            diffenValues.add(value);
        }
        return diffenValues;
    }
}
