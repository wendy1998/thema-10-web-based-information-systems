package nl.bioinf.wbvandermeulen.form_checks.experiment;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.chrono.ChronoLocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ExperimentalDataChecker {
    private final Map<String, String> attributes;
    private static final List<String> TEXT_ATTRIBUTES = Arrays.asList("analist", "slabel", "stype", "pnaam", "note");
    private static final List<String> DATE_ATTRIBUTES = Arrays.asList("sdatbinnen", "pgebdatum");
    private static final List<String> TIME_ATTRIBUTES = Arrays.asList("stijdbinnen");

    public ExperimentalDataChecker(HttpServletRequest request) {
        this.attributes = collectAttributes(request);
    }

    private Map<String, String> collectAttributes(HttpServletRequest request) {
        Map<String, String> filledAttributes = new HashMap<>();
        Map<String, String[]> attributes = request.getParameterMap();
        for (Map.Entry<String, String[]> attr: attributes.entrySet()) {
            for(String value: attr.getValue()) {
                if(!value.isEmpty()) {
                    if(!(value.equals("Submit"))) {
                        filledAttributes.put(attr.getKey(), value);
                    }
                }
            }
        }
        return filledAttributes;
    }

    public boolean checkMandatory() {
        Mandatory[] mandatory = Mandatory.values();
        for(Mandatory id: mandatory) {
            if(!attributes.containsKey(id.name)) {
                return false;
            }
        }
        return true;
    }

    public String checkFieldType() {
        Set<String> filledFields = getAttributeIds();
        for(String id: filledFields) {
            if(id.equals("revision")) {
                continue;
            } else if(TEXT_ATTRIBUTES.contains(id)) {
                if(!checkString(id)) {
                    return "Attribuut " + id + " is niet van het goede format. (Moet text zijn, zonder ; erin)";
                }
            } else if(DATE_ATTRIBUTES.contains(id)) {
                String message = checkDate(id);
                if(message != null) {
                    return message;
                }
            } else if(TIME_ATTRIBUTES.contains(id)) {
                if(!checkTime(id)) {
                    return "Tijd van binnenkomst sample ligt in de toekomst";
                }
            } else {
                if(!checkNumeric(id)) {
                    return "Attribuut " + id + " is niet van het goede format. (Moet numeriek zijn)";
                }
            }
        }
        return null;
    }

    private boolean checkString(String id) {
        String value = attributes.get(id);
        return !value.contains(";");
    }
    private boolean checkNumeric(String id) {
        String value = attributes.get(id);
        try {
            Float.parseFloat(value);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }

    private String checkDate(String id) {
        String value = attributes.get(id);
        LocalDate parse = LocalDate.parse(value, DateTimeFormatter.ISO_DATE);
        String[] components = value.split("-");
        // check whether year, month and day are given
        if(components.length != 3) {
            return "Geen goed datum format.";
        }
        // check lengths of elements
        if(components[0].length() != 4) {
            return "geen goed datum format";
        } else {
            if(components[1].length() != 2) {
                return "geen goed datum format";
            } else {
                if(components[2].length() != 2) {
                    return "geen goed datum format";
                }
            }
        }
        // check whether date lies in the past/is today
        LocalDate now = LocalDate.now();
        if(now.isBefore(parse)) {
           return "Datum ligt in de toekomst";
        }

        return null;
    }

    private boolean checkTime(String id) {
        String date = attributes.get("sdatbinnen");
        LocalDate parse = LocalDate.parse(date, DateTimeFormatter.ISO_DATE);
        LocalDate nowDate = LocalDate.now();
        if(!nowDate.isAfter(parse)) {
            String value = attributes.get(id);
            LocalTime now = LocalTime.now(ZoneId.of("Europe/Amsterdam"));
            LocalTime valueTime = LocalTime.parse(value, DateTimeFormatter.ISO_TIME);
            return valueTime.isBefore(now);
        }
        return true;
    }

    public String checkDiffen() {
        List<String> diffenAttributes = Diffen.getDiffenAttributes();
        if(!attributes.keySet().containsAll(diffenAttributes)) {
            return "Niet alle velden van diffen zijn ingevuld";
        }
        List<Double> diffenValues = Diffen.getDiffenValues(attributes);
        double sum = diffenValues.stream().mapToDouble(Double::doubleValue).sum();
        OptionalDouble max = diffenValues.stream().mapToDouble(Double::doubleValue).max();

        if(max.getAsDouble() < 1) {
            if(sum <  0.999990 || sum > 1.0000001) {
                return "Diffen velden zijn geen 100% samen";
            }
        } else {
            if(sum < 99.999990 || sum > 100.0000001) {
                return "Diffen velden zijn geen 100% samen";
            }
        }

        return null;
    }

    public Set<String> getAttributeIds() {
        return attributes.keySet();
    }

    public Map<String, String> getAttributeMap() {
        return this.attributes;
    }



    public boolean diffenNeeded() {
        Set<String> filledFields = getAttributeIds();
        for(String id: filledFields) {
            if(id.startsWith("dif")) {
                return true;
            }
        }
        return false;
    }
}
