package nl.bioinf.wbvandermeulen.form_checks;

import nl.bioinf.wbvandermeulen.dao.DatabaseException;
import nl.bioinf.wbvandermeulen.dao.MyAppDao;
import nl.bioinf.wbvandermeulen.dao.MyAppDaoMySQL;
import nl.bioinf.wbvandermeulen.users.User;

import java.util.ArrayList;
import java.util.List;

public class CheckNewUser {
    private final String userName;
    private final String password1;
    private final String password2;
    private final int teachersCode;

    public CheckNewUser(String userName, String password1, String password2) {
        this.userName = userName;
        this.password1 = password1;
        this.password2 = password2;
        this.teachersCode = 0;
    }

    public CheckNewUser(String userName, String password1, String password2, int teachersCode) {
        this.userName = userName;
        this.password1 = password1;
        this.password2 = password2;
        this.teachersCode = teachersCode;
    }

    public String checkPassword() {
        if (!(password1.equals(password2))) {
            return "Wachtwoorden zijn niet gelijk";
        }

        // check if password isn't already in use
        MyAppDao dao = MyAppDaoMySQL.getInstance();
        try {
            dao.connect();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        try {
            List<String> passwords = dao.getPasswords();
            if(passwords.contains(password1)) {
                dao.disconnect();
                return "Wachtwoord bestaat al";
            }
        } catch (DatabaseException e) {
            e.printStackTrace();
            dao.disconnect();
            return "Er gaat iets fout, probeer het nog een keer.";
        }

        return null;
    }

    public String checkUsername() {
        // check if username isn't already in use
        MyAppDao dao = MyAppDaoMySQL.getInstance();
        try {
            dao.connect();
        } catch (DatabaseException e) {
            e.printStackTrace();
            return "Er gaat iets fout, probeer het nog een keer.";
        }
        try {
            List<String> usernames = dao.getUsernames();
            if(usernames.contains(userName)) {
                dao.disconnect();
                return "Username bestaat al";
            }
        } catch (DatabaseException e) {
            e.printStackTrace();
            dao.disconnect();
            return "Er gaat iets fout, probeer het nog een keer.";
        }

        return null;
    }

    public boolean checkTeachersCode(int teachersCode) {
        return this.teachersCode == teachersCode;
    }

    public boolean hasTeachersCode() {
        return this.teachersCode != 0;
    }
}
