package nl.bioinf.wbvandermeulen.servlets;

import nl.bioinf.wbvandermeulen.dao.DatabaseException;
import nl.bioinf.wbvandermeulen.dao.MyAppDao;
import nl.bioinf.wbvandermeulen.dao.MyAppDaoMySQL;
import nl.bioinf.wbvandermeulen.form_checks.CheckNewUser;
import nl.bioinf.wbvandermeulen.users.Role;
import nl.bioinf.wbvandermeulen.users.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "SignupServlet", urlPatterns = "/signup.do")
public class SignupServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = request.getParameter("username");
        String password1 = request.getParameter("password1");
        String password2 = request.getParameter("password2");
        String teachersCode = request.getParameter("docent_code");

        RequestDispatcher view;
        // check values
        CheckNewUser checker;
        if(!teachersCode.equals("")) {
            try {
                Integer.parseInt(teachersCode);
            } catch (NumberFormatException e) {
                request.setAttribute("teacherError", "Verkeerde code");
                view = request.getRequestDispatcher("create_account.jsp");
                view.forward(request, response);
            }
            checker = new CheckNewUser(userName, password1, password2, Integer.parseInt(teachersCode));
        } else {
            checker = new CheckNewUser(userName, password1, password2);
        }

        String message = checker.checkPassword();
        if(message != null) {
            request.setAttribute("passwordError", message);
            view = request.getRequestDispatcher("create_account.jsp");
            view.forward(request, response);
            return;
        }

        String username = checker.checkUsername();
        if(username != null) {
            request.setAttribute("usernameError", username);
            view = request.getRequestDispatcher("create_account.jsp");
            view.forward(request, response);
            return;
        }

        int teacher_code = Integer.parseInt(getServletContext().getInitParameter("teachers_code"));
        if((checker.hasTeachersCode()) && !checker.checkTeachersCode(teacher_code)) {
            request.setAttribute("teacherError", "Verkeerde code");
            view = request.getRequestDispatcher("create_account.jsp");
            view.forward(request, response);
            return;
        }

        // insert new user into database
        MyAppDao dao = MyAppDaoMySQL.getInstance();
        try {
            dao.connect();
            if(checker.hasTeachersCode()) {
                dao.insertUser(userName, password1, Role.TEACHER);
            } else {
                dao.insertUser(userName, password1, Role.TEAM);
            }
        } catch (DatabaseException e) {
            e.printStackTrace();
        } finally {
            dao.disconnect();
        }
        request.setAttribute("succesfullSignup", "Account aanmaken succesvol.");
        view = request.getRequestDispatcher("create_account.jsp");
        view.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("create_account.jsp");
        view.forward(request, response);
    }
}
