package nl.bioinf.wbvandermeulen.servlets;

import nl.bioinf.wbvandermeulen.dao.DatabaseException;
import nl.bioinf.wbvandermeulen.dao.ExperimentalDataInserter;
import nl.bioinf.wbvandermeulen.form_checks.experiment.Diffen;
import nl.bioinf.wbvandermeulen.form_checks.experiment.ExperimentalDataChecker;
import nl.bioinf.wbvandermeulen.users.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

@WebServlet(name = "ProcessDataServlet", urlPatterns = "/processdata")
public class ProcessDataServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        RequestDispatcher view;
        if(session.getAttribute("user") == null) {
            view = request.getRequestDispatcher("index.jsp");
        } else {
            LocalTime now = LocalTime.now(ZoneId.of("Europe/Amsterdam"));
            LocalDate today = LocalDate.now(ZoneId.of("Europe/Amsterdam"));
            view = request.getRequestDispatcher("res_invoer.jsp");

            // check data
            ExperimentalDataChecker checker = new ExperimentalDataChecker(request);
            if(!checker.checkMandatory()) {
                request.setAttribute("errorMessage", "Niet alle verplichte velden zijn ingevuld.");
                view.forward(request, response);
            }

            String message = checker.checkFieldType();
            if(message != null) {
                request.setAttribute("errorMessage", message);
                view.forward(request, response);
                return;
            }
            if(checker.diffenNeeded()) {
                message = checker.checkDiffen();
                if(message != null) {
                    request.setAttribute("errorMessage", message);
                    view.forward(request, response);
                    return;
                }
            }

            // insert data into database
            ExperimentalDataInserter inserter = new ExperimentalDataInserter(checker.getAttributeMap(), today, now,
                    (User) session.getAttribute("user"));
            List<Object> preparedData = inserter.prepareDataInsert();
            try {
                inserter.insertData(preparedData);
            } catch (DatabaseException e) {
                e.printStackTrace();
                request.setAttribute("errorMessage", e.getMessage());
            }
        }
        request.setAttribute("successMessage", "Datainvoer gelukt.");
        view.forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        RequestDispatcher view;
        if(session.getAttribute("user") == null) {
            view = request.getRequestDispatcher("index.jsp");
        } else {
            view = request.getRequestDispatcher("res_invoer.jsp");
            request.setAttribute("errorMessage", "Dit werkt alleen via het form, niet via de url");
        }

        view.forward(request, response);
    }
}
