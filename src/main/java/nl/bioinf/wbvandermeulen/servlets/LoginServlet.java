package nl.bioinf.wbvandermeulen.servlets;

import nl.bioinf.wbvandermeulen.users.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "LoginServlet", urlPatterns = "/login.do")
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        RequestDispatcher view;
        User u = (User) session.getAttribute("user");
        if (u != null) {
            view = request.getRequestDispatcher("res_invoer.jsp");
        } else {
            //authenticate
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            u = new User(username, password);
            final User authenticatedUser = u.authenticate();
            if (authenticatedUser == User.GUEST) {
                final String errorMessage = "Verkeerde inlognaam of wachtwoord";
                request.setAttribute("errorMessage", errorMessage);
                view = request.getRequestDispatcher("index.jsp");
            } else {
                session.setAttribute("user", authenticatedUser);
                view = request.getRequestDispatcher("res_invoer.jsp");
            }
        }
        view.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        RequestDispatcher view;
        if(session.getAttribute("user") == null) {
            view = request.getRequestDispatcher("index.jsp");
        } else {
            view = request.getRequestDispatcher("res_invoer.jsp");
        }
        view.forward(request, response);
    }
}
