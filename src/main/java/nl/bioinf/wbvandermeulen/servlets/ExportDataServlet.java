package nl.bioinf.wbvandermeulen.servlets;

import nl.bioinf.wbvandermeulen.dao.DatabaseException;
import nl.bioinf.wbvandermeulen.dao.MyAppDao;
import nl.bioinf.wbvandermeulen.dao.MyAppDaoMySQL;
import nl.bioinf.wbvandermeulen.users.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "ExportDataServlet", urlPatterns = "/datadownload")
public class ExportDataServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        RequestDispatcher view;
        if(session.getAttribute("user") == null) {
            view = request.getRequestDispatcher("index.jsp");
        } else {
            MyAppDao dao = MyAppDaoMySQL.getInstance();
            view = request.getRequestDispatcher("show_res.jsp");
            try {
                dao.connect();
            } catch (DatabaseException e) {
                e.printStackTrace();
                request.setAttribute("errorMessage", "Export mislukt, probeer het nog een keer.");
            }

            String filename = request.getParameter("outfile");
            String type = request.getParameter("outtype");
            if(type.equals("Alles")) {
                try {
                    dao.exportData(filename, (User) session.getAttribute("user"), type);
                } catch (DatabaseException e) {
                    e.printStackTrace();
                    request.setAttribute("errorMessage", "Export mislukt, probeer het nog een keer.");
                }
                request.setAttribute("successMessage", "Export naar file " + filename + " is gelukt.");
            } else {
                try {
                    dao.exportData(filename, (User) session.getAttribute("user"), "today");
                } catch (DatabaseException e) {
                    e.printStackTrace();
                    request.setAttribute("errorMessage", "Export mislukt, probeer het nog een keer.");
                }
                request.setAttribute("successMessage", "Export naar file " + filename + " is gelukt.");
            }
            dao.disconnect();
        }
        view.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("user");
        RequestDispatcher view;
        if (u != null) {
            view = request.getRequestDispatcher("show_res.jsp");
        } else {
            view = request.getRequestDispatcher("index.jsp");
        }
        view.forward(request, response);
    }
}
