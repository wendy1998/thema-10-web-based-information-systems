package nl.bioinf.wbvandermeulen.servlets;

import com.google.gson.Gson;
import nl.bioinf.wbvandermeulen.dao.DatabaseException;
import nl.bioinf.wbvandermeulen.dao.MyAppDao;
import nl.bioinf.wbvandermeulen.dao.MyAppDaoMySQL;
import nl.bioinf.wbvandermeulen.users.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ShowDataServlet", urlPatterns = "/overview")
public class ShowDataServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String json = null;
        if(session.getAttribute("user") == null) {
            RequestDispatcher view = request.getRequestDispatcher("index.jsp");
            view.forward(request, response);
        } else {
            MyAppDao dao = MyAppDaoMySQL.getInstance();
            try {
                dao.connect();
                List<List<String>> data = dao.getExperimentalOverview((User) session.getAttribute("user"));
                dao.disconnect();
                json = new Gson().toJson(data);
                // Return results as JSON
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
            } catch (DatabaseException e) {
                e.printStackTrace();
                dao.disconnect();
            }
        }
    }
}
