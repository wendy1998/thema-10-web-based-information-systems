package nl.bioinf.wbvandermeulen.users;

public enum Role {
    GUEST,
    TEAM,
    TEACHER
}
