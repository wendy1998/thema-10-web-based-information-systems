package nl.bioinf.wbvandermeulen.users;

import nl.bioinf.wbvandermeulen.dao.DatabaseException;
import nl.bioinf.wbvandermeulen.dao.MyAppDao;
import nl.bioinf.wbvandermeulen.dao.MyAppDaoMySQL;

import java.util.Objects;

public class User {
    public static final User GUEST;
    private String userName;
    private String password;
    private Role role;

    static {
        GUEST = new User("Guest", "", Role.GUEST);
    }

    public User() {}

    public User(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public User(String userName, String password, Role role) {
        this.userName = userName;
        this.password = password;
        this.role = role;
    }

    public User authenticate() {
        MyAppDao dao = MyAppDaoMySQL.getInstance();
        try {
            dao.connect();
            final User user = dao.getUser(userName, password);
            dao.disconnect();
            return user;
        } catch (DatabaseException e) {
            e.printStackTrace();
            dao.disconnect();
            return User.GUEST;
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", role=" + role +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(getUserName(), user.getUserName()) &&
                Objects.equals(getPassword(), user.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserName(), getPassword());
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() { return role; }
}
