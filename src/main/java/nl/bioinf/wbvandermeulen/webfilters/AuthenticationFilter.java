package nl.bioinf.wbvandermeulen.webfilters;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = {"/res_invoer.jsp", "/show_res.jsp"}) //use "/*" for catch-all
public class AuthenticationFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        /*not interesting here*/
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            HttpServletRequest req = ((HttpServletRequest)request);
            System.out.println("[AuthenticationFilter] Intercepted URL: " + req.getRequestURL().toString());
            final HttpSession session = req.getSession();
            if (session.getAttribute("user") == null) {
                System.out.println("[AuthenticationFilter] no authenticated user: redirecting to /login");
                ((HttpServletResponse)response).sendRedirect("/index.jsp");
            } else {
                System.out.println("[AuthenticationFilter] authenticated status checked; user= " + session.getAttribute("user"));
                try {
                    chain.doFilter(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return;
    }

    @Override
    public void destroy() {
        /*not interesting here*/
    }
}
