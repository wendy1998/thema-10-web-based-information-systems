package nl.bioinf.wbvandermeulen.experiment_classes;

public class Patient {
    private int pnum;
    private String name;
    private String birthDate;
    public static final Patient NA;

    static {
        NA = new Patient(0, "NA", "0000-00-00");
    }

    public Patient(int pnum, String name, String birthDate) {
        this.pnum = pnum;
        this.name = name;
        this.birthDate = birthDate;
    }

    // getters
    public int getPnum() {
        return pnum;
    }

    public String getName() {
        return name;
    }

    public String getBirthDate() {
        return birthDate;
    }
}
