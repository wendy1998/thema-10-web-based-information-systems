package nl.bioinf.wbvandermeulen.experiment_classes;

public class Sample {
    private int label;
    private String type;
    private String dateAdded;
    private String timeAdded;
    private int patient;
    public static final Sample NA;

    static {
        NA = new Sample(0, "NA", "0000-00-00", "00:00", 0);
    }

    public Sample(int label, String type, String dateAdded, String timeAdded, int patient) {
        this.label = label;
        this.type = type;
        this.dateAdded = dateAdded;
        this.timeAdded = timeAdded;
        this.patient = patient;
    }

    // getters
    public int getLabel() {
        return label;
    }

    public String getType() {
        return type;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public String getTimeAdded() {
        return timeAdded;
    }

    public int getPatient() {
        return patient;
    }
}
