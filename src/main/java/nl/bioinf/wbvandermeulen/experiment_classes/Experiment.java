package nl.bioinf.wbvandermeulen.experiment_classes;

import nl.bioinf.wbvandermeulen.users.User;

import java.util.List;

public class Experiment {
    private User team;
    private String analist;
    private String date_result;
    private String time_result;
    private int sample;
    private double cr_ur_lit;
    private double cr_ur_dag;
    private double cr_bloed;
    private double ur_lit;
    private double ur_dag;
    private double hematocriet;
    private double hemoglobine;
    private int diffen;
    private double salicylzuur;
    private double ldh;
    private double mcv;
    private double mch;
    private double mchc;
    private String note;
    private boolean revision;

    public Experiment(User team, String analist, String date_result,
                      String time_result, int sample, double cr_ur_lit,
                      double cr_ur_dag, double cr_bloed, double ur_lit,
                      double ur_dag, double hematocriet, double hemoglobine,
                      int diffen, double salicylzuur, double ldh,
                      double mcv, double mch, double mchc, String note, boolean revision) {
        this.team = team;
        this.analist = analist;
        this.date_result = date_result;
        this.time_result = time_result;
        this.sample = sample;
        this.cr_ur_lit = cr_ur_lit;
        this.cr_ur_dag = cr_ur_dag;
        this.cr_bloed = cr_bloed;
        this.ur_lit = ur_lit;
        this.ur_dag = ur_dag;
        this.hematocriet = hematocriet;
        this.hemoglobine = hemoglobine;
        this.diffen = diffen;
        this.salicylzuur = salicylzuur;
        this.ldh = ldh;
        this.mcv = mcv;
        this.mch = mch;
        this.mchc = mchc;
        this.note = note;
        this.revision = revision;
    }

    public User getTeam() {
        return team;
    }

    public String getAnalist() {
        return analist;
    }

    public String getDate_result() {
        return date_result;
    }

    public String getTime_result() {
        return time_result;
    }

    public int getSample() {
        return sample;
    }

    public double getCr_ur_lit() {
        return cr_ur_lit;
    }

    public double getCr_ur_dag() {
        return cr_ur_dag;
    }

    public double getCr_bloed() {
        return cr_bloed;
    }

    public double getUr_lit() {
        return ur_lit;
    }

    public double getUr_dag() {
        return ur_dag;
    }

    public double getHematocriet() {
        return hematocriet;
    }

    public double getHemoglobine() {
        return hemoglobine;
    }

    public int getDiffen() {
        return diffen;
    }

    public double getSalicylzuur() {
        return salicylzuur;
    }

    public double getLdh() {
        return ldh;
    }

    public double getMcv() {
        return mcv;
    }

    public double getMch() {
        return mch;
    }

    public double getMchc() {
        return mchc;
    }

    public String getNote() {
        return note;
    }

    public boolean isRevision() {
        return revision;
    }
}
