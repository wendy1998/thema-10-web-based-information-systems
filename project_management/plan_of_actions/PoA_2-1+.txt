week 2:
- update PoA/gant chart
- create login/signup pages - after lesson 2.1
- create login/signup servlets - after lesson 2.1
- create java code for User objects - after lesson 2.1

week 3:
- Write Java code for the addition of data to the database - lesson 3.1
- create Servlet for result form - lesson 3.1
- Write Java code for getting data from the database and showing that on the website - lesson 3.1/3.2
- Create page for the showing of data on the website - lesson 3.2
- Write Java code for checking the form - lesson 3.2
- Write non-functional html/xml code for time sensitive samples - lesson 3.2/3.3
- Make form dynamic with javascript - lesson 3.3
- Create form and servlet for adding time sensitive samples - lesson 3.3
- Talk to client about wishes/progress - after lesson 3.3

week 4/5:
- Think about what has to go in the report
- Think about what has to go in the presentation

week 6:
- implement time sensitive samples - lesson 6.1
- Talk to client about product - between lesson 6.1 and 6.2
- make product better with the wishes of the client in mind - lesson 6.2
- create report - lesson 6.2/6.3

week 7:
- Create presentation - lesson 7.1
- styling of the webpage - lesson 7.2
- present project - lesson 7.3